import os
import telegram
import json
from google.oauth2 import service_account
from googleapiclient import discovery
import base64

bot = telegram.Bot(token=os.environ["TELEGRAM_TOKEN"])

asd = {
  "type": "service_account",
  "project_id": "",
  "private_key_id": "",
  "private_key": "",
  "client_email": "",
  "client_id": "",
  "auth_uri": "",
  "token_uri": "",
  "auth_provider_x509_cert_url": "",
  "client_x509_cert_url": ""
}

def webhook(request):
    if request.method == "POST":
        try:
            update = telegram.Update.de_json(request.get_json(force=True), bot)
            chat_id = update.message.chat.id
            if 'test1' in update.message.text:
                reply_text = 'test'
            elif 'get devices' in update.message.text:
                reply_text = get_index(json.dumps(asd), 'iot-school-2', 'us-central1', 'coap-demo')
            elif 'get state' in update.message.text:
                reply_text = get_state(json.dumps(asd), 'iot-school-2', 'us-central1', 'coap-demo', update.message.text.split()[2])
            elif 'set config' in update.message.text:
                reply_text = set_config(json.dumps(asd), 'iot-school-2', 'us-central1', 'coap-demo', update.message.text.split()[2], ' '.join(update.message.text.split()[3:]))
            elif 'config all' in update.message.text:
                reply_text = config_all(json.dumps(asd), 'iot-school-2', 'us-central1', 'coap-demo', ' '.join(update.message.text.split()[2:]))
            else:
                reply_text = update.message.text
            bot.sendMessage(chat_id=chat_id, text=reply_text)
        except AttributeError:
            return "ok"
    return "ok"

def get_client(service_account_json):
    """Returns an authorized API client by discovering the IoT API and creating
    a service object using the service account credentials JSON."""
    api_scopes = ['https://www.googleapis.com/auth/cloud-platform']
    api_version = 'v1'
    discovery_api = 'https://cloudiot.googleapis.com/$discovery/rest'
    service_name = 'cloudiotcore'
    temp = json.loads(service_account_json)
    credentials = service_account.Credentials.from_service_account_info(temp)
    scoped_credentials = credentials.with_scopes(api_scopes)
    discovery_url = '{}?version={}'.format(discovery_api, api_version)
    return discovery.build(service_name, api_version, discoveryServiceUrl=discovery_url, credentials=scoped_credentials)

def get_index(service_account_json, project_id, cloud_region, registry_id):
    client = get_client(service_account_json)
    registry_path = 'projects/{}/locations/{}/registries/{}'.format(project_id, cloud_region, registry_id)
    return client.projects().locations().registries().devices().list(parent=registry_path).execute().get('devices', [])

def get_state(service_account_json, project_id, cloud_region, registry_id, device_id):
    try:
        client = get_client(service_account_json)
        registry_path = 'projects/{}/locations/{}/registries/{}'.format(project_id, cloud_region, registry_id)
        devices = client.projects().locations().registries().devices().list(parent=registry_path).execute().get('devices', [])
        for device in devices:
            if device.get('id') == device_id:
                device_path = '{}/devices/{}'.format(registry_path, device_id)
                devices = client.projects().locations().registries().devices()
                state = devices.states().list(name=device_path, numStates=1).execute()
                state_base64_str = state['deviceStates'][0]["binaryData"]
                return base64.b64decode(state_base64_str).decode('utf-8')
    except AttributeError:
        return "AttributeError"
    return "AttributeError"

def set_config(service_account_json, project_id, cloud_region, registry_id, device_id, config):
    try:
        client = get_client(service_account_json)
        registry_path = 'projects/{}/locations/{}/registries/{}'.format(project_id, cloud_region, registry_id)
        devices = client.projects().locations().registries().devices().list(parent=registry_path).execute().get('devices', [])
        for device in devices:
            if device.get('id') == device_id:
                device_path = 'projects/{}/locations/{}/registries/{}/devices/{}'.format(project_id, cloud_region, registry_id, device_id)
                config_body = {'binaryData': base64.urlsafe_b64encode(config.encode('utf-8')).decode('ascii')}
                return client.projects().locations().registries().devices().modifyCloudToDeviceConfig(name=device_path, body=config_body).execute()
    except AttributeError:
        return "AttributeError"
    return "AttributeError"

def config_all(service_account_json, project_id, cloud_region, registry_id, config):
    client = get_client(service_account_json)
    registry_path = 'projects/{}/locations/{}/registries/{}'.format(project_id, cloud_region, registry_id)
    devices = client.projects().locations().registries().devices().list(parent=registry_path).execute().get('devices', [])
    for device in devices:
        device_path = 'projects/{}/locations/{}/registries/{}/devices/{}'.format(project_id, cloud_region, registry_id, device.get('id'))
        config_body = {'binaryData': base64.urlsafe_b64encode(config.encode('utf-8')).decode('ascii')}
        client.projects().locations().registries().devices().modifyCloudToDeviceConfig(name=device_path, body=config_body).execute()
    return 'devices configured'